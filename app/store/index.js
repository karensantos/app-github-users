import Vue from "nativescript-vue";
import Vuex from "vuex";
import Api from "@/utils/api";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {},
    actions: {
        async loadUsers({ commit }, user) {
            const { data } = await Api.get(`/users/${user}`);
            console.log(data);
        }
    },
    mutations: {},
    getters: {}
});
